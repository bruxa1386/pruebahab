//nResult para cantidad de perfiles que queremos buscar

const nResult = '100'
const url = `https://randomuser.me/api/?results=${nResult}`


const axios = require('axios');


(async () => {
    const datos = await axios.get(url)
    let processedData = {}
    processedData = datos.data.results.map(line => {
        processedData.usuario = line.login.username
        processedData.nombre = line.name.first
        processedData.apellido = line.name.last
        processedData.genero = line.gender
        processedData.pais = line.location.country
        processedData.email = line.email
        processedData.enlace_foto = line.picture.medium
        return processedData
    })
    console.log(processedData);
})()