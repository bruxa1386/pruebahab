//introducimos el número binario
let numero = 10101010101
const base = 2      //introducimos 10 para numero decimal y 2 para binarios

const binarioDecimal = numero => {
    numero = numero.toString().split('').reverse()
    resultado = 0

    for (i = 0; i < numero.length; i++) {

        if (numero[i] === '1') {

            resultado += Math.pow(base, i)

        }

    }
    return resultado
}
if (base === 2) {
    console.log(`El número binario ${numero} equivale en decimal a => ` + binarioDecimal(numero));
} else {
    console.log(`El número decimal ${numero} equivale en binario a => ` + numero.toString(2));
}
