let segundos = 0
let minutos = 0
let horas = 0
let dias = 0
console.log('hola');
const contador = () => {
    const intervalo = 5
    segundos = intervalo + segundos
    if (segundos === 60) {
        minutos++
        segundos = 0
    }
    if (minutos === 60) {
        horas++
        minutos = 0
    }
    if (horas === 24) {
        dias++
        horas = 0
    }
    console.log(`Este programa se ejecutó hace ${dias} días ${horas} horas ${minutos} minutos ${segundos} segundos`);
}

setInterval(contador, 5000)