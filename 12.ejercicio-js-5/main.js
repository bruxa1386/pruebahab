const url = 'https://rickandmortyapi.com/api/'

const axios = require('axios');

(async () => {
    const datos = await axios.get(url)
    const datosEpisodios = await axios.get(datos.data.episodes)
    const personajesEpisodiosEnero = datosEpisodios.data.results
        .filter(item => item.air_date.includes('January'))
        .map(item => item.characters)

    let personajes = new Set()
    for (i = 0; i < personajesEpisodiosEnero.length; i++) {
        for (j = 0; j < personajesEpisodiosEnero[i].length; j++) {
            personajes.add(personajesEpisodiosEnero[i][j])
        }

    }
    personajes = Array.from(personajes)
    personajes = personajes.map(url => axios.get(url))

    Promise.all(personajes).then(datos => {
        let listaNombres = []
        datos.map(item => listaNombres.push(item.data.name))
        console.log(listaNombres)
    })

})()